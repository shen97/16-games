## 16个C++游戏

- 图形库`SFML`
- VS2019



### 配置方式

在官网下载SFML库[Download (SFML) (sfml-dev.org)](https://www.sfml-dev.org/download.php)，我这里选择Windows

VS2019中：

1. 项目>>配置属性>>C/C++>>附加包含目录>>添加SFML中的include路径

2. 项目>>配置属性>>常规>>附加库目录>>添加SFML中的lib文件路径

3. 项目>>Debug>>连接器>>输入

   ```
   sfml-audio-d.lib
   sfml-graphics-d.lib
   sfml-system-d.lib
   sfml-window-d.lib
   sfml-network-d.lib
   ```

   如果是Release，添加：

   ```
   sfml-audio.lib
   sfml-graphics.lib
   sfml-system.lib
   sfml-window.lib
   sfml-network.lib
   ```

4. 配置环境变量：

   ```
   CPLUS_INCLUDE_PATH
   C:\Env\SFML-2.6.1-windows-vc16-64-bit\include
   
   LIBRARY_PATH
   C:\Env\SFML-2.6.1-windows-vc16-64-bit\lib
   ```

5. 出现找不到dll的错误，就把bin文件中的dll复制到生成的exe同级目录中

### 其他

第15个游戏会使用一个2d库，需要再去加载一个头文件

### 参考

- [超详细！SFML库vs2022配置教程-CSDN博客](https://blog.csdn.net/Hsianus/article/details/130727463)
- [erincatto/box2d: Box2D is a 2D physics engine for games (github.com)](https://github.com/erincatto/box2d)

